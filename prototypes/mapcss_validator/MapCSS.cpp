#include <iostream>
#include <fstream>
#include <iterator>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

namespace qi = boost::spirit::qi;
namespace chw = qi::standard_wide;

enum SelectorTypeEnum
{
	Node,
	Way,
	Relation,
	Area,
	Line,
	Canvas,
	Any
};

enum BinaryTypesEnum
{
	Equal,
	Unequal,
	SameAs,
	LessThen,
	GreaterThen,
	LessEqual,
	GreaterEqual
};

enum UnaryTypesEnum
{
	Not,
	Minus
};

enum UnitTypesEnum
{
	Pixel,
	Points,
	Percent
};



struct SelectorTypes_ : qi::symbols<wchar_t, SelectorTypeEnum>
{

	SelectorTypes_()
	{
		add
			(L"node", Node)
			(L"way", Way)
			(L"relation", Relation)
			(L"area", Area)
			(L"line", Line)
			(L"canvas", Canvas)
			(L"*", Any)
			;
	}


}SelectorTypes;

struct BinaryTypes_ : qi::symbols<wchar_t, BinaryTypesEnum>
{

	BinaryTypes_()
	{
		add
			(L"=", Equal)
			(L"!=", Unequal)
			(L"=~", SameAs)
			(L"<", LessThen)
			(L">", GreaterThen)
			(L"<=", LessEqual)
			(L">=", GreaterEqual)
			;
	}


}BinaryTypes;

struct UnaryTypes_ : qi::symbols<wchar_t, UnaryTypesEnum>
{

	UnaryTypes_()
	{
		add
			(L"!", Not)
			(L"-", Minus)
			;
	}


}UnaryTypes;


struct UnitTypes_ : qi::symbols<wchar_t, UnitTypesEnum>
{

	UnitTypes_()
	{
		add
			(L"px", Pixel)
			(L"pt", Points)
			(L"%", Percent)
			;
	}


}UnitTypes;

template<typename ItType>
struct MapCSSGrammar : qi::grammar<ItType>
{
	MapCSSGrammar()
		:	MapCSSGrammar::base_type(mainRule, "MapCSS-MainGrammar"),
			mainRule("MapCSS-MainRule")
	{
		using qi::lit;
		using qi::double_;
		using qi::int_;
        using qi::on_error;
        using qi::fail;
		using qi::accept;
        using namespace qi::labels;
        using boost::phoenix::construct;
        using boost::phoenix::val;
	
		rule_string = +(chw::alnum | L'-' | L'_' | L'#' | L'.');

		// Declaration
		rule_sizes = (int_ > UnitTypes) % L',';
		rule_color = lit(L"rgb(") > int_ > L',' > int_ > L',' > int_ > L')';
		rule_url = L"url(" > rule_string > L')';
		rule_eval = L"eval(" > rule_string > L')';

		rule_style = rule_string >> L':' > (rule_string | rule_sizes | rule_color | rule_url | rule_eval);

		rule_styleset = (rule_style % L';') > -lit(L';');

		// Selectors
		rule_tag = rule_string % L':';

		rule_condition = (rule_tag > BinaryTypes > rule_string) | (UnaryTypes > rule_tag) | rule_tag;

		rule_zoom = L"|z" > (int_ > -(L'-' > -int_) | (L'-' > int_));

		rule_class = (lit(L':') | lit(L'.')) > +chw::alpha;

		rule_subselector = SelectorTypes > -rule_zoom > *(L'[' > rule_condition > L']') > -( (lit(L'!') > rule_class) | rule_class);

		rule_selector = +rule_subselector;


		// Document
		rule_cssrule = rule_selector % L','
						> L'{'
						> rule_styleset
						> L'}';

		mainRule = *rule_cssrule;

		on_error<fail>
        (
            mainRule
          , std::cout
                << val("Error! Expecting ")
                << _4                               // what failed?
                << val(" here: \"")
                << construct<std::string>(_3, _2)   // iterators to error-pos, end
                << val("\"")
                << std::endl
        );
	}
	
	qi::rule<ItType> rule_string;

	qi::rule<ItType> rule_selector;
	qi::rule<ItType> rule_subselector;
	qi::rule<ItType> rule_zoom;
	qi::rule<ItType> rule_condition;
	qi::rule<ItType> rule_tag;
	qi::rule<ItType> rule_class;


	qi::rule<ItType> rule_styleset;
	qi::rule<ItType> rule_style;
	qi::rule<ItType> rule_sizes;
	qi::rule<ItType> rule_color;
	qi::rule<ItType> rule_url;
	qi::rule<ItType> rule_eval;
	
	qi::rule<ItType> rule_cssrule;
	qi::rule<ItType> mainRule;
};






int main()
{
	typedef boost::spirit::basic_istream_iterator<wchar_t> IteratorType;
	//typedef std::istream_iterator<wchar_t, wchar_t> IteratorType;
	MapCSSGrammar<IteratorType> mapscc_grammar;


	std::wifstream filestream("test.txt");

	if(!filestream)
	{
		std::cout << "Failed to load file!";
		std::cin.get();
		return 1;
	}

	IteratorType eof;
	IteratorType it(filestream);

	bool r = qi::parse(it, eof, mapscc_grammar);

	if(r && it == eof)
	{
		std::cout << "File is valid!";
	}else
	{
		std::wcout << L"Error: file is invalid!";
	}

	std::cin.get();

	return 0;
}
