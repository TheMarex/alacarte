canvas {
   background-color: #ffffea;
   default-points: false;
   default-lines: false;
 }
 relation|z2-30[boundary=administrative][admin_level=7]
 {
   width: 1;
   color: black;
   opacity: 1;
   fill-color: orange;
   fill-opacity: 0.3;
   font-size: 16;
   text-color: green;
   text: admin_level;
   text-position: center;
 }
 relation[boundary=administrative][admin_level=8]:Closed
 {
   width: 2;
   color: black;
   opacity: 1;
   fill-color: blue;
   fill-opacity: 0.3;
   font-size: 16;
   text-color: red;
   text: admin_level;
   text-position: center;
 }
 relation[boundary=administrative][admin_level=6]
 {
   width: 1;
   color: black;
   opacity: 1;
   fill-color: brown;
   fill-opacity: 0.3;
   font-size: 16;
   text-color: white;
   text: admin_level;
   text-position: center;
 }
