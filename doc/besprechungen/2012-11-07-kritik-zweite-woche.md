# Kritik der zweiten Woche #

## Allgemein ##
* Makro für Benamung verbessern und vereinheitlichen - Done
    ** Bringe Struktur in die Namen und die dann in das Label, nicht die Nummern - Done
    ** Nummeriere dann automatisch durch - Done
* Auf dreistellige oder vierstelluge Nummern festlegen - Done
* FM <=> FW, also FMuss und FWunsch oder eben andersrum, MussF und WunschF aber nicht wechseln - Done
* Benutze Glossaries-Package - Done!
* In den funktionalen Anforderungen nicht "xy können" schreiben, sondern richtig von den Funktionen schreiben -Done
* Produkteinsatz und Produktumgebung hinter funktionale Anforderungen schieben -Done 
* Deutsche Trennung einschalten - siehe: http://forum.ubuntuusers.de/post/3756042/ -Done
* FM3200 aber auch Rest: Beschreibe immer den Basisfall der Anfrage einer Kachel und drückt euch präzise aus => keine nebulösen Ausdrücke - Done
* Zum erweitern betone die fettgedruckten Bestandteile des Pflichtenhefts:
    ** Zielbestimmungen
    ** Produktfunktionen
    ** Systemmodell
    ** Benuzterobefläche
    ** Testfälle und -szenarien

## Einleitung ##
* Erwähnen, dass es zwei Teilprogramme gibt. - Done
* Hier fehlt noch, dass wir nicht einfach 3 verschiedene hart codierte Stile haben, sondern die Stile dynamisch über MapCSS einbinden - Done
## Kriterien ##
* Beispiel wie wirs machen sollen:
    ** "Die Software kann Kartendaten von Karlsruhe in ein aufbereitetes Zwischenformat übersetzen."
    ** nicht "kann" etc. => "Die Software übersetzt Kartendaten von Karlsruhe in ein aufbereitetes Zwischenformat."
    ** "Die Software" ist auch sowieso klar => "Übersetzen der Kartendaten von Karlsruhe in ein aufbereitetes Zwischenformat."
    ** "aufbereitet" sagt nichts aus => "Übersetzen der Kartendaten von Karlsruhe in ein Zwischenformat."
* Einfügen:
    ** Multithreading bzw. nenne "System blockiert nicht bei mehreren Anfragen" als Kriterium nennen. - Done
* MK200 und MK300 vertauschen - Done
* Zoomstufenformel: 
    ** Ins Glossar verschieben - Done
    ** Mercatorprojektion und SlippyMapFormat(Done) ins Glossar und dort erklären -Done
    ** "Auf Zoomstufe 0 ist die ganze Karte eine Kachel, danach wird mit jeder Stufe eine Kacheln in 4 aufgeteilt" (siehe Aufgabenstellung der anderen) -Done
* MK400 vor MK300 - Done
* MK400 zu kompliziert => Jedes Feature einzeln als Unterpunkte oder eine Tabelle mit Wunsch- und Mussfeatures. - Done
* WK300 <=> MK300 ( WK300 ist trivial, passt eher zu User Interface bzw. funktionalen Anforderungen) - Done
* AK neuformulieren - Done
* User Interface: - Done
    ** Konfigurationsdatei, Kommandozeilenparameter: Was ist einstellbar? Jede Einstellmöglichkeit nenen, aber nicht auf Namen in der Konfigurationsdatei oder der Parameter festlegen.
    ** Was? => Was kann am Programm verändert und eingestellt werden?
    ** Wo? => ini-Datei und Kommandozeilenparameter
* max. 2^32 Entitäten (falls wir den Identifieransatz benutzen)
* Labelplacement: nicht exakt, Heuristik. - Done

## Produktdaten ##
* Offener, sonst sind wir auf zB Cairo festgelegt <- Für Anwender egal, eigentlich Implementierungsdetail - Done
* Schon im Pflichtenheft ein Stück vorwegnehmen: Modularisierung, um z.B. andere Backends für den Renderer (Grafikkarte, OpenGL, ...) zu ermöglichen - Done
* OSM-XML, CSS, Zwischendaten, Daten im RAM beschreiben. - Done

## Funktionen ##
* Anforderungen aufteilen nach Vorverarbeitungsprogramm und Renderer/Auslieferungsprogramm. - Done
* Produktfunktionen enthalten Kriterien - Done
* "Eingabedaten"-Überschrift wäre ein Kanditat zur Ersetzung mit "Programm 1" (bzw. einem sinnvolleren Namen) - Done
* Funktionen sind Operationen!!! nicht "einstellbar", "machbar" sondern Grundfunktionen erklären - Done
* FM3200: Nicht auf PNG-Format festlegen, könnten ja z.B. auch svg rendern => offener formulieren - Done
* Komponenten(Dennis)/Operationen(Veit) - Komponenten wären zum Beispiel Stilparsing oder die Serverkomponente
* kann, soll usw. sollen raus, wir schreiben hier keine Forderungsliste an uns selbst
    ** nicht "veränderbar machen / können" sondern - Das System tut X - Done
* FA immer wie Funktionsnamen: calcX() -> X berechnen - Done
* Alle MapCSS-Eigenschaften die wir unterstützen wollen getrennt nach Wunsch und Muss auflisten - Done
* Beispielfunktionen:
     * Funktion: Stylesheet einstellen
     * Funktion: Kartenausschnitt auswählen
     * Funktion: Kartenausschnitt zeichen
     * Funktion: Stylesheet aus URL ableiten


* Tabelle formatieren:
    ** Tabellenunterschrift - Done
    ** Außenumrandung weglassen - Done
    ** Am besten als fließobjekt - Done
    ** Verschieben zu Kriterien
    ** Überschriften besser machen - Done
    ** Komplett in Fließtext umwandeln?
* FM2400 vll auch andere Formate unterstützen, die einfach rausfallen. - Done
* Funktion: Rendererkomponente zeichnet Kacheln aus den Daten => keine Details wie "Übergibt an den HTTP-Server" - Done
* Modularität anpreisen als "Unsere Software ist das Erweiterbarste seit geschnitten Brot" - Done
* Bei Erweiterbarkeit nichts vom CSS schreiben, eben eher Modularisierung mit tauschbarem Parser, Renderer.. - Done
* 5.1.3 :
    ** Konsistenz bzw. Auslagern und Verweisen (FM3100 inkonsistentes SlippyMap-Format, nur einmal (im Glossar) erwähnen) - Done
    ** Keine institutionellen Begriffe (Auftraggeber in FM3300) - Done
    ** Version und Subset von HTTP definieren (wahrscheinlich HTTP1.1, GET) - Done
    ** HTTP-gzip unterstützen? -> sinnvoll? PNG-Bilder sind zlib komprimiert, also nein? - Done
* FM3300 Warteschlange Implementierung
    ** Hinzufügen: Default Kachel falls zu lange Wartezeit - Done
    ** Wartezeit nicht einstellbar, Standard HTTP-Timeout beachten - Done
* FM4100 "Das System liest die Konfigurationsdatei ein und übernimmt die Einstellungen ins Programm" (nix mit ...einstellbar)
* Höhenlinien komplett weg - Done


## Nichtfunktionales ##
* NM2100 -> Robustheit - Done
* NM3100: unterstützen definieren - Done
* Erweiterbarkeit: Komponenten austauschen. - Done
* Wunschanforderung: Anwendung hält auch mit den Kartendaten Baden-Württembergs alle anderen Kriterien ein. - Done

## Diagramme ##
* Usecases raus - Done
* Grobes Übersichtsdiagramm:
    ** was heißen die Pfeile? => beschriften mit fließenden Daten, oder Benutztrelation machen
    ** Bildunterschrift fehlt (\figure-Umgebung)
    ** Paket benennen
    ** MVC einbauen (Controller fehlt), aber vielleicht sollten Regelanwender und Renderer doch direkt miteinander reden.
    ** MVC zum Bild schreiben
    ** MVC schreiben in Musskrit. und / oder zur Einleitung

## Produktdaten ##
* Ist alles Blödsinn, stattdessen: - Done
    ** CSS-Daten
    ** OSM-XML-Daten
    ** Zwischenformat
    ** Elemente unserer Kartendaten im Hauptspeicher (Knoten&Kanten)


# Implementierungstipps #
* keine pointer, sondern identifier(int)
* Zoomstufen:
    ** Stufen 0-10 oder 0-12: Einmal vorrendern und im Cache / auf der Festplatte halten (2 mio. Kacheln)
    ** Geometrische Datenstruktur benutzen, Rechtecksabfragen stellen und die Zoomstufen 12-18 dynamisch berechnen
* Stylesheets:
    ** Stylesheetordner überwachen und bei neuem Stylesheet die Stufen 0-12 vorrendern
    ** während der Vorberechnung eine Default-Kachel ausliefern
* Geometrische Struktur für Kanten
    ** Speichere für Ways die Kanten-IDs in den Sektoren die der Way schneidet und nicht nur die Nodes zwischen denen der Way liegt in den Sektoren der Nodes.
    ** Über die Kanten-ID wird dann auf das eigentliche Kantenobjekt zugegriffen
    ** Speichere Objekte in Array und vergebe IDs statts Zeiger
    ** In jedem Blatt/Sektor gewisse Anzahl an Kanten, dann nicht mehr unterteilen (zB 200) => nicht wie sonst oft "1 Punkt <=> 1 Sektor"
    ** tricks: Query optimieren: Nach Typ sortieren, ein Subtree für Waldwege, anderer für Straßen...
    ** Gleich mehrere Bäume für die Typen wie Ländergrenzen, Straßen, Feldwege, ...
    ** Nicht ums Auslagern auf Festplatte kümmern, auch Baden-Württemberg passt komplett in den RAM
* Renderer rendert:
    ** Punkte
    ** Linien
    ** Rechtecke
    ** Polygone
    ** Icons
* => Aber auch Text!
    ** Labelplacement ist NP-vollständig
    ** Löse nicht exakt sondern verwende Heuristik und schreibe das von vorneherein rein
    ** nachforschen
