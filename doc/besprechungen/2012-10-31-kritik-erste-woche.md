# Mitschrieb vom 31.10.2012 #

## Allgemeine Hinweise zur Programmstruktur ##

Aufteilung in zwei Programme:

1. Macht den Import und ordnet die Daten so an, wie wir sie brauchen.
    * in welchem Format?
    * was müssen wir aus den osm-daten rausziehen? => Praktisch alles
    * Geomatrische Datenstrukturen (nicht ins Pflichtenheft, aber im Kopf)
    * explizite Vorverarbeitung
    * vorbereitet für kommende Rechtecksanfragen
2. Greift auf die Zwischendaten zu, rendert diese und liefert das Ergebnis aus.

Gemeinsame Komponente für das Zwischenformat auslagern.
Der Server ist ein statisches, statusloses Offline-Tool.

### Architektur ###
1. Anfrage von außen
2. Antrage an interne Datenstruktur
3. Maloperationen sammeln
4. In Cairo werfen
5. Ausliefern


## Kritik der Betreuer am Pflichtenheft (alles angewendet) ##

Unsere Betreuer legen viel wert auf kurze und präzise Sätze.
Auch unsere Oma (die aber glücklicherweise 1958 ihren Abschluss in Informatik gemacht hat) muss es verstehen.

### Allgemeine Kritik ###
 * Wir haben die Aufgabenstellung nicht bzw. nicht genau genug gelesen
 * Wir sollen z.B. für die Numerierungen bei funktionalen / nicht funktionalen Anforderungen Latex-Makros benutzen - DONE!
 * Wir benutzen Begriffe inkonsequent (z.B. Karten, Kartendaten, Kartenmaterial). Wir müssen uns einmal auf einen Begriff festlegen, im Glossar erläutern und nicht im Dokument variieren. - DONE!
   * Definiere Karte und Update
   * Kartenmaterial sind Kartenrohdaten
   * aus der OSM bekommen wir Kartendaten, keine Karten
   * Kacheln, Tile, Ausschnitt
 * Zuerst sollen wir die Kriterien aufschreiben, daraus folgen automatisch unsere Anforderungen. - DONE!
 * Was in der Aufgabenstellung fettgedruckt ist ist _wichtig_. Das sind die Pflichtenheftbestandteile, auf die Wert gelegt wird. - DONE!
 * Kriterien: Was will ich? Funktionen: Wie? - DONE!

### Detailanmerkungen ###
#### Titelseite ####
Hinzufügen:

* PSE - DONE!
* Institut - DONE!
* Semester - DONE!

#### Einleitung ####
Unsere Software - DONE!

 * malt ne Karte
 * malt nicht nur eine Karte, sondern unterschiedliche Karten
 * malt nicht nur statisch ein paar Karten, sondern individualisierbare Karten
 * ...

#### Zielbestimmung (Wunsch- und Musskriterien) ####
 * Musskriterien: Anforderungen der Betreuer und direkte implizierte Anforderungen daraus. - DONE!
 * Musskriterien 1:1 wie in der Aufgabenstellung übernehmen und nicht härter machen - DONE!
 * Wunschkriterien: Rest, was auch immer wir vielleicht schaffen werden - DONE!
 * DE rendern -> alle Kacheln Deutschlands rendern (nicht eine D-Karte) - DONE!
 * Weiterverarbeitung streichen - DONE!
 * Hinzufügen: unterstützte Formen, Farben, Alpha (Transparenz) - DONE!
 * optionale Sachen mit * markieren
 * Beispiel: "unterschiedliche Stylesheets" - DONE!
    1. Wie eine Karte gezeichnet wird wird durch ein Stylesheet bestimmt
    2. Die Stylesheets können durch Benutzer gewählt werden
    3. In einem Stylesheet ist auswählbar:
        * .
        * .
        * .
    4. Ein Stylesheet ist eine kontextfreie Grammatik und spezifiziert bei http:// in Version Z

#### Produkteinsatz ####
 * Zielgruppe sind nur Serverbetreiber - DONE!
 * "individualisiert" besser ausformulieren => auf MapCSS beziehen - DONE!
 * Bei Abhängigkeiten nichts von einem initsystem schreiben, im Zweifel sind wir nur ein einfaches Kommandozeilenprogramm das mit einem Aufruf ohne Parameter gestartet wird - DONE!

#### Produktumgebung ####
 * Orgware: TCP/IP zum Benutzer, kein Gigabit dazuschreiben, eher Ethernet-Netzwerk - DONE!
 * OSM-Geodaten-Standard existiert nicht => schreibe OSM-Format und xml, wird benutzt zum Import von Kartendaten - DONE!
 * Für jede Schnitstelle: In welche Richtung? Was? - DONE!
 * Öffentliche API -> Webserver (HTTP), der das "Slippy-URL-Schema" implementiert (dazu die Slippy-Doku mit rein) - DONE!
 * Parameterübergabe rein, aber keine genaue Definition - DONE!
 * CLI, init/systemd rauswerfen - DONE!

#### funktionale Anforderungen ####
 * funktionale Anforderungen sind Funktionen, keine Eigenschaften (nicht "aktualisierbar" etc.) - DONE!
 * nicht "verstehen" -> parsen ist der Fachbegriff - DONE!
 * Jede Einstellmöglichkeit eine funktionale Anforderung - DONE!
 * primitive, atomare Anforderungen, low level, das big picture steht bei den Wunsch- und Musskriterien - DONE!
 * Beispiele was bei uns so fehlt: - DONE!
     * Funktion: Stylesheet einstellen
     * Funktion: Kartenausschnitt auswählen
     * Funktion: Kartenausschnitt zeichen
     * Funktion: Stylesheet aus URL ableiten
 * MapCSS: Auflisten aller unterstützten Eigenschaften (getrennt nach Wunsch und Muss) - DONE!
 

#### nichtfunktionale Anforderungen ####
 * Keine man-page - DONE!
 * downtime weg - DONE!
 * Warteschlange weg - DONE!
 * Ungültige Eingaben (HTTP, Stylesheets) sollen ignoriert werden. - DONE!
 * Benutzbarkeit: Unser Programm kann einfach von der Kommandozeile gestartet werden - DONE!
 * Wollen wir Karten unterstützen, die nicht in den Hauptspeicher passen? - DONE!

## Für nächstes Mal / anlesen ##
 * Unsere Kriterien!!
 * Eingabeformat (OSM, MapCSS) <- Grammatiken
 * Was sind Parser?
