# Besprechung vom 12.12.2012, letzte vor Abgabe des Entwurfs #
alles erledigt
## Architekturdiagramm ##
    * "Architektur" in "Allgemeine Architektur" benennen - Done
    * dort Komponentennamen benutzen und nicht Klassen - Done
## Übersichts-Klassendiagramm ##
    * Verbindung zwischen StylesheetManager und TileIdentifier - Done
## Umbenennungen ##
    * "Object" in "GeoObject" - Done
    * "StyleMap" umbennenen in "RenderAttributes" - Done
## TileIdentifier ##
    * enum Format als Klassenbox oder im UML-Diagramm dahinter "enum" schreiben - Done
    * "Create liefert null wenn URL nicht gültig" erwähnen - Done
    * Attribut-Beschreibung: Rückgabe ändern von String in Enum - Done
## Sequenzdiagramme ##
    * Einleitung schreiben - Patrick
    * Doppelpunkte vor Aufrufen entfernen (insk)
    * Rückgabewerte im Sequenzdiagramm einzeichnen
    * Konstruktoraufrufe auf den Kasten - DOne
    * HTTPAnfagen an Server => listen() weglassen Done
    * Abb. 3: Erwähnen, dass hier nur ein Job erstellt (und nicht abgearbeitet) wird
            Was passiert bei weiteren Anfragen? (Loop)
    * Abb. 4: Kasten abgeschnitten, Nullpointer wenn TileIdentifier-Konstruktor fehlschlägt.
    * Wir benutzen dann den Standardkonstruktor - Done
    * Abb. 6 Rechtschreibung 
    * Abb. 7 vorrendern
    * Abb. 8 
## Klassenbeschreibungen diverses ##
    * Glossarverlinkungen machen - Done
    * Klassenbeschreibung in 3 Sektionen, z.B. : -Done
        * gemeinsame Komponenten
        * Importer
        * Server
    * Node, Way, Relation erben von Object: im UML-Kasten mit Doppelpunkt notieren
    * 4.2 gls{Objekt}e => glspl - Done
    * 4.4 errorCode => HttpStatusCode - Done
    * Configklasse noch aktualisieren - Done
    * private Attribute überall weg, sieht man über set und get - Done
    * getLocation() bei Node: WGS84 angeben - Shimon
    * getter / setter nicht beschreiben wenn trivial, am Ende der Beschreibung als Liste - Done
    * Punkte in Konstruktoren wenn Konstruktor ewig lang. (Diagramme) - Done
    * in Beschreibung vom RequestManager sagen, dass synchronisiert ist (und die auch so verbessern) - Done
    * getTile/Cache anmerken, dass hier synchronisiert wird - Done
## Allgemeines ##
    * Layout der Diagramme überarbeiten
        * Doppelpunkte
    * Mercator-Projektion erwähnen
    * Glossarmakros wieder mit Pfeil - Done
    * HTTPRequest: nicht errorCode sondern statusCode - Done
    * Grafiken mit \figure, Caption und Verweis auf die Grafik im Text - Done
    *Boost und Cairo Glossar Einträge - Done
    *Boost und Cairo referenzieren- Done
    *Glossar Eintrag für OSM-Tags- Done
## Ganz am Ende ##
    * Einmal durchgehen und alle Grafiken skalieren.
    * Einmal durchlesen und alle Begriffe an Umbenennungen anpassen

	* Klassendiagramm ihnen am Wochenende zusenden, sie drucken uns das in A0 aus und wir können vorm Kolloquium dadrin mit Farben rummalen
	* Kolloquium ist am Donnerstag um 9:45 Uhr
