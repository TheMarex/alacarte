## Besprechung vom 27.02.2013 ##
	* Wir haben noch weiße Flecken auf der Karte: Flughafen / Sportplatz-Areas zeichnen
	* kleine Labels überdecken große => andersrum sortieren, auf Zoomstufe 1 ist Egenberg oder sodas oberste Label
	* „Leute lieben es, wenn der Computer ihnen sagt, was man tun soll“ => „Das hier ist Kunst, vom Computer!“
		* (oberste Zoomstufe mit unserem Label Placement)
	* Halten wir eigentlich unsere Performance-Nichtfunktionale-Kriterien ein?
	* Rhein ist area=riverbank als auch teilweise eine Relation type=multipolygon, zeichnen wir das beides?
	* Bäume im Stylesheet zeichnen
	* OpenGL-Renderer-Backend?
	* KD-Baum-Bild im Abschlussvortrag
		* Farben unterschiedlich nach Rekursionsstufe, Flächen oder Linien
		* Wie sieht das mit Ways aus?
	* Seite, wo Farbfanatiker Farben und Paletten sharen: http://www.colourlovers.com/
	* 5-Passende-Farben-Generator: https://kuler.adobe.com/
	* Konsolenausgabe reduzieren beim Vorrendern
	* Highway Shields (Mapnik kann die nämlich nicht so g'scheit, und Dennis ist Fan von Highway Shields)
	* Für die Demo: Haltestellennamen und -Linien einzeichnen
	* algo2.iti.kit.edu/verkehr

### Plots verbessern ###
	* Box Plots (Verteilungen zeichnen)
	* Median einzeichnen, je nachdem pro Zoomlevel
	* Logarithmische Skalen oft sinnvoll
	* Einheiten anpassen, ms und kNodes wenn sinnvoll
	* Abhängigkeit pro Zoomlevel
	* Vorgerenderte Kacheln in anderer Farbe
	* Nullpunkt unten links, nicht negativ

### Validierungsdokument ###
	* "Verhalten war im _Entwurf_ so nicht vorgesehen", nicht "in der Implementierung"
	* Fixed Point Arithmetik => Fixed Point Coordinates
	* Bei den Fehlern: Komponente dazuschreiben, so genau wie im Entwurf
	* Makros für Fehler und Testfälle angleichen..?
	* Beim Cachefehler: Beschreibung verbessern
	* Beim "Geodata verschluckt Nodes": Beschreibung verbessern
	* Fehlerbehebung: Mal Vergangenheit, mal Präsens => angleichen

Alles entweder getan oder als Issue eingetragen.


### Was wir tun müssten um den Renderer in tatsächlichen Gebrauch zu bekommen ###
	* PostgreSQL-Datenbank für dynamische Daten(-aktualisierung) als Backend, mit Cache Busting
	* Meta-Kachel-Rendering, 32x32 Kacheln auf einmal auf der Grafikkarte rendern
		* bei anderen Anfragen an dieses Gebiet das feststellen, dass das gebiet schon gezeichnet wird und auf fertig warten (muss der Cache machen)
