# Besprechung vom 19.12.2012, 1. Implementierung #

## Allgemeines ##
	* Verantwortlichkeiten schaffen, wer ist für welche Komponente zuständig?
	* So früh wie möglich sollen alle Interfaces was tun
		* Mock-Kacheln: Zoomlevel + Koordinate reindendern
	* Entwicklungsumgebung schleunigst einrichten
	* am Ende: Final-Tag setzen, aber trotzdem tarball mit Implementierungsdokument wegschicken

	* Die andere Gruppe hat modelio für die UML-Sachen benutzt: http://www.modeliosoft.com/
	* SourceCode-Font ist eins der wenigen guten Dinge aus Adobe, spezialisiert auf Quellcode:
		* http://www.golem.de/news/open-source-font-source-code-verschoenert-source-code-1209-94795.html
		* http://sourceforge.net/projects/sourcecodepro.adobe/

## GANTT-Chart ##
	* Meilensteine: z.B. Geodada: 1. Mockup-Version in 3 Tagen, voll funktionsfähig in 6 Wochen
	* Wir sind 6 Leute - habe immer 6 Balken parallel
	* Komponenten in Unterkomponenten aufteilen soweit sinvoll
		* z.B. dann kein Balken für Gesamtkomponente, sondern nur für Unterkomponenten

## Debug-Tipps ##
	* Bei Memory Leaks: valgrind, z.B. valgrind --tool=mem-check <programmaufruf>
		* Build mit debug-Symbolen zeigt dann auch die Zeilennummern an
	* Kachelkoordinaten klein auf die Kacheln rendern hilft beim debuggen



Implementierungstipps:
----------------------
* Painter's Algorithm oder z-order-sort, damit kann man die Ebenensortierung beim Renderer machen
* http://data6.lustich.de/bilder/l/21894-translate-server-error.jpg
	
* Finger weg von stl:map, ist ein RotSchwarzBaum mit O(log n) -> boost UnorderedMap ist eine richtige HashTable mit O(1)
* Konsequent SmartPointer benutzen
* Pfadüberwachung im StylesheetManager: inotify
* Auflistung der Anderungen
* Häufig miteinander abstimmen, wer hat was gecodet und was wird wer als nächstes tun?
* boost unit tests benutzen
* Wenn wir einen Bug finden -> Test dazu schreiben, er wird wahrscheinlich nochmal auftreten
* Nur einen branch höher promoten, was die Tests auch besteht oder wenn das mit den anderen abgestimmt wurde
* Benutze branches: Master (stabil) <- dev (testing) <- eigene branches pro person oder untergruppe / feature branches
* So früh wie möglich integrieren (alles zusammenstöpseln künnnen und lauffähig haben, wenn auch langsam)
* Continous integration server aufsetzen (Jenkins)
* boost::ProgramOptions für Kommandozeilenparameter, es gibt was ähnliches in boost auch für ini-files
* Tipp: Exceptions sind nicht teuer
* Pfadüberwachung: unter Linux gibts den Systemcall inotify() dafür
* Queue im RequestManager: 2 interne Queues, für jede Priorität eine, und dan boost::scope_lock benutzen
* Cache: für jedes Stylesheet eine eigene Hashtabelle, um dann beim Löschen einfach die Tabelle wegwerfen zu können.
* HTTP-Parsen in Request: Stumpf zeile für Zeile durchgehen, und wenn sie nicht interessiert dann wegschmeißen
