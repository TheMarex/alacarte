## Aufgabenstellung Validierungsphasendokument ##

- Dokumentenumfang ca. 20 Seiten

- Protokoll der gefundenen (nichttrivialen) Fehler, aufgesplittet nach Fehlerklassen (Usability, Performance, ...)
	- Fehler: Beschreibung des Fehlers
	- Bedingung: Wann tritt der Fehler auf, wie ist er zu reproduzieren
	- Ursache: Warum tritt der Fehler auf, was war das tatsächliche Problem im Code?
	- Behebung: Was musste geändert werden, damit er nicht mehr auftritt?

- Testen der Testfälle und -Szenarien aus dem Pflichtenheft,
  bzw. wie und warum diese angepasst wurden (falls geschehen).
  Tests sollen soweit wie möglich automatisiert werden (z.B.
  mit JUnit), ist dies nicht möglich, müssen sie manuell
  ausgeführt werden.

- Korrektheitstests: Überlegt euch, z.B. wie ihr die
  Korrektheit eurer Algorithmik testen könnt, z.B. durch
  vorgenerierte Testfälle, die mit einem einfacheren aber
  langsameren Algorithmus erzeugt wurden, wobei die
  Ergebnisse in eine Datei geschrieben wurden.

- Coverage: Zwei getrennte Statistiken einmal für die
  automatisierten Testfälle alleine und einmal inklusive
  interaktiver Benutzung des Programm (die Szenarien aus dem
  Pflichtenheft sind ein guter Einstieg, zusätzlich sollte man
  dann 'mal alles was die GUI hergibt' ausprobieren und die
  Daten für die Coverage nebenbei sammeln).

- Gemeinsame Coverage von händischen und automatisierten Tests

- OPTIONAL: Skalierungs- und Belastungs-Tests:
  Messt z.B. wie viel Zeit eine Anfrage an die geometrische
  Datenstruktur durchschnittlich benötigt.
  Hier ist es vielleicht auch mal sinnvoll Graphen verschiedener Größe
  auszuprobieren und durch die asymptotische Komplexität des jeweiligen
  Algorithmus zu dividieren. Kommt dabei ungefähr eine waagrechte
  Linie raus, dann ist die Skalierung sozusagen plausibel. Aber dieser
  Punkt ist eher "nice to have", wenn der Rest fertig ist.

- Wenn ihr in bisherigen Phasen schon etwas gemacht habt, dass sich hier
  wiederverwenden lässt, könnt ihr diese Ergebnisse natürlich in dieser
  Phase verwenden, ohne die Arbeit komplett neu machen zu müssen.
