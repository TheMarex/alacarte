#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# clean up old glossary index files
rm validierungsdokument.glo
rm validierungsdokument.xdy
# clean up old makeglossaries file
rm validierungsdokument.gls

pdflatex -halt-on-error -interaction=nonstopmode validierungsdokument
makeglossaries validierungsdokument
pdflatex -halt-on-error -interaction=nonstopmode validierungsdokument
