/*
 * Importer
 * Sequenzieller Importer als Klasse ist ok
 * Element einlesen
 * Typ erkennen
 * parseNode/Way/Relation
 * Elemente in HashTable OSMID -> Element stecken
 * danach:
 * Ways und Relations durchgehen
 * Existenz der verwendeten NodeIDs / WayIDs prüfen, wenn verletzt die Referenz rauswerfen
 * Relations auf Konsistenz prüfen (inner / outer), wenn verletzt Relation rauswerfen
 * Nodes, Ways, Relations in vector stecken. Die Stelle im vector ist seine neue id.
 * Übergebe die vectors an Geodata.insert(..)
 * Gebe Geodata zurück
 */
/*
 * Parst die OSM-XML-Daten und speichert sie in der Geodata Klasse.
 */
class Importer {
private:
	Configuration* config;
public:
	Importer(Configuration* config);
	Geodata* importXML();
};
/*
 * Speichert alle Daten und kann sich selbst in eine Datei schreiben und aus einer Datei laden.
 * Alle Zugriffe laufen mithilfe von IDs über diese Klasse.
 */
class Geodata {
private:
	vector<Way>* ways;
	vector<Node>* node;
	vector<Relation>* relations;
public:
	/*
	 * Fertig gefilterte und umbenannt Daten.
	 */
	void insertNodes(vector<Node>* nodes);
	void insertWays(vector<Way>* ways);
	void insertRelations(vector<Relation>* relations);
	vector<int>* getNodeIDs(const Rect& rect);
	vector<int>* getWayIDs(const Rect& rect);
	vector<int>* getRelationIDs(const Rect& rect);
	Node* getNode(int id);
	Way* getWay(int id);
	Relation* getRelation(int id);
	void load(const string& path);
	void save(const string& path);
};
/*
 *    -------------------------------------------------------------------
 */
/*
 * Server
 * Vorrendern: Alle vorzurendernden Kacheln weren ganz normal (mit niedriger Priorität) in die RequestQueue eingefügt.
 * int main(args) {
 *    Configuration* c = new Configuration(args);
 *    Geodata* data = Geodate::load(c.getGeodataPath());
 *    Renderer* renderer = new Renderer(data);
 *    RequestManager* m = new RequestManager(10);
 *    Cache* cache = new Cache();
 *    StylesheetManager* manager = StylesheetMonitor(c, q);
 *    Server* s = new Server(c, m);
 *    s.listen();
 * }
 */
/*
 * Läd alle konfigurierbaren Optionen aus der Konf-Datei
 * und Kommandozeilen-Parametern und stellt sie anderen Klassen bereit.
 */
class Configuration {
public:
	Configuration(boost::option_description* desc, int argc, char** argv);
	template<typename T>
	T get(const string& key);
};

/*
 * Wartet auf neue HTTP-Anfragen.
 * Wenn eine neue Anfrage kommt wird ein Request erstellt
 * und an den RequestManager übergeben.
 */
class HttpServer {
private:
	Configuration* config;
	RequestManager* manager;
public:
	Server(Configuration* config, RequestManager* manager);
	void listen();
	void quit();
};
/*
 * Der eigentliche "Controller".
 * Kapselt die gesamte HTTP-Funktionalität, zieht die richtigen
 * Daten aus Geodata und steckt sie dann in das Stylesheet
 * und den Renderer.
 *
 * Process-Methode in Worker auslagern?
 */
class HttpRequest {
private:
	Socket* socket;
public:
	/* Bearbeitet die HTTP-Anfrage -> url raus holen. */
	static HttpRequest* Create(const string& httpMessage, Socket* socket);
	
	// Ließt die URL aus der HTTP Get Anfrage aus.
	const string& getURL();
	void answer(const Tile* tile, int statusCode = 200);
};
/*
 * Der RequestManager nimmt Requests vom Server entgegen,
 * und verteilt sie auf Threads.
 */
class RequestManager {
private:
	Geodata *data;
	Renderer *renderer;
	Cache *cache;
	StylesheetManager *ssm;
	boost::asio::io_service jobPool;
	Configuration* config;
public:
	RequestManager( Configuration* config,
					Geodata *data,
				 Renderer *renderer,
				 Cache *cache,
				 StylesheetManager *ssm);
	void enqueue(HttpRequest *r);
	void enqueue(TileIdentifier* ti);
	Geodata* getGeodata();
	StylesheetManager* getStylesheetManager();
	Cache* getCache();
	Renderer* getRenderer();
};
class PreRenderJob {
private:
	TileIdentifier* tileIdentifier;
	RequestManager* manager;
public:
	PreRenderJob(TileIdentifier* tileIdentifier, RequestManager* manager);
	void process();
};
/*
 * Wird vom RequestManager erstellt und an seine Worker verteilt.
 */
class UserRequestJob {
private:
	HttpRequest* req;
	RequestManager* manager;
public:
	UserRequestJob(HttpRequest* request, RequestManager* manager);
	void process()
	{
		TileLocator tl = TileLocator::parse(reg.getURL());
		Tile* tile = cache.getTile(tl);
		if (!tile.isRendered()) {
			Rect r = computeRect(tl.getX(), tl.getY(), tl.getZoom());
			nodeIDs = data.getNodeIDs(r);
			wayIDs = data.getWayIDs(r);
			relIDs = data.getRelationIDs(r);
			Stylesheet stylesheet = ssm.getStylesheet(tl);
			RenderAttributes map = stylesheet.match(nodeIDs,
											wayIDs,
								   relationIDs);
			renderer.renderTile(map, tile);
		}
		req.answer(tile);
	};
};
/*
 * Die TileLocator kapselt alle Daten die über die URL der Tile-Anfrage
 * übergeben wurden.
 */
class TileIdentifier{
public:
	enum Format{
		PNG,
		JPEG,
		GIF,
		SVG,
		SVGZ
	};
private:
	int x;
	int y;
	int zoom;
	string styleSheetpath;
	Format imageFormat;
public:
	static TileIdentifier* Create(const string& url);
	TileIdentifier(int x, int y, int zoom, string styleSheetpath, Format imageFormat);
	int getX();
	int getY();
	int getZoom();
	Format getImageFormat();
	const string& getStylesheetPath();
};
/*
 * Der Cache speichert häufig verwendete Tiles.
 * Alle Tiles müssen über getTile() erzeugt werden,
 * der Cache behält eine Referenz und kann sich so merken
 * wie oft eine Tile angefragt wurde.
 * Vorgerenderte Kacheln werden am Zoomlevel erkannt und dauerhaft gespeichert.
 */
class Cache {
private:
	Configuration* config;
public:
	Cache(Configuration* config);
	
	// Must not return null, returns new Tile() if not found.
	Tile* getTile(TileIdentifier* tl);
	void deleteTiles(Stylesheet* sheet);
};
/*
 * Kapselt die Daten die zu einer Kachel gehören.
 * Tiles werden vom nur Cache erzeugt.
 */
class Tile {
private:
	// Fertige PNG/SVG/whatever Datei
	char* image;
	int lenght;
	TileIdentifier* id;
	Tile(TileIdentifier* id);
public:
	bool isRendered();
	char* getImage();
	void setImage(char *image);
	int getLength();
	TileIdentifier* getIdentifier();
};
/*
 *  Grundobjekt. Stellt Umrisse und Tags bereit.
 */
class GeoObject {
private:
	map<string, string> tags;
public:
	GeoObject(const map<string, string>& tags);
	const map<string, string>& getTags();
};
/*
 * Ein einzelner Node.
 */
class Node : public GeoObject {
private:
	Point location;
public:
	Node(const Point& location, const map<string, string>& tags);
	const Point& getLocation();
};
/*
 * Ein Zusammenschluss von Nodes.
 */
class Way : public GeoObject {
private:
	vector<int> nodeIDs;
public:
	Way(const vector<int>& nodeIDs, const map<string, string>& tags);
	const vector<int>& getNodeIDs();
};
/*
 * Ein Zusammenschluss von Ways oder Nodes.
 */
class Relation : public GeoObject {
private:
	vector<int> nodeIDs;
	vector<int> wayIDs;
	map<int, string> nodeRoles;
	map<int, string> wayRoles;
public:
	Relation(const vector<int>& nodeIDs, 
			 const map<int, string>& nodeRoles,
		  const vector<int>& wayIDs,
		  const map<int, string>& wayRoles,
		  const map<string, string>& tags);
	const vector<int>& getWayIDs();
	const vector<int>& getNodeIDs();    
	const string& getWayRole(int id);
	const string& getNodeRole(int id);
};
/*
 * Parst neue Stylesheets-Dateien, erzeugt ihnen Regeln
 * und speichert sie in einem Stylesheet-Objekt.
 * Bereits geparste Stylesheets werden gecachet.
 */
class StylesheetManager {
private:
	Configuration* config;
	RequestManager* manager;
public:
	/* Bei neuem Stylesheet wird in den RequestManager
	 * pseudo Requests eingefügt, welche die gewünschten Tiles
	 * vorrendern.
	 * pseudo Request = Request mit leerer answer Methode
	 */
	StylesheetManager(Configuration* config);
	Stylesheet* getStylesheet(TileIdentifier ti);

	void startStylesheetObserving(RequestManager* manager);
};
/*
 * Parste das Stylesheet
 */
class Stylesheet {
private: 
	Geodata* data;
public:    
	static Stylesheet* Load(const string& path, Geodata* data);
	void match( vector<int>* nodeIDs,
				vector<int>* wayIDs,
			 vector<int>* relIDs,
			 RenderAttributes* styleMap);
};
/*
 * Bildet ids auf den Style dieser Objekte ab.
 */
class RenderAttributes {
private:
	map<int, Style> wayStyles;
	map<int, Style> nodeStyles;
public:
	map<int, Style>& getWayMap();
	map<int, Style>& getNodeMap();
};
/*
 * Speichert die key:value kombinationen einer Regel bzw. später
 * die auf ein Objekt zutreffenden key:value kombinationen.
 * Problem: Eval? Funktioniert so nicht, da wir es nicht repräsentieren können.
 */
class Style {
public:
	void overmerge(Style* style);
	Color fill_color; //2x
	int z_index;
	double width;
	Color color;    
	double casing_width;
	Color casing_color;
	double font_size;
	Color text_color;
	Point text_position;
	string text;
};
/*
 * Sortiert die Objekte in richtiger Zeichenreihenfolge,
 * sucht sich die passende Style-Attribute und zeichnet diese in eine Kachel.
 */
class Renderer {
private:
	Geodata *data;
public:
	Renderer(Geodata *data);
	void renderTile(RenderAttributes& map, Tile* tile);
};
/* Optimierung:
 * Jedes GeoObject bekommt eine ID gespeichert, die angibt, welche Tags es hatte (Keys und Values) (eine ID für alle Values) und in eine Map wird die Zuordnung ID->List<Tag> gespeichert. Beim Parsen eines Stylesheets werden alle IDs durchgegangen und es werden alle Regeln auf die Tags gematcht. In einer neuen Map (eine pro Stylesheet) wird dann nur noch gespeichert ID->Style. (REs lassen sich hier vorberechnen). Diese Map wird im Stylesheet-Objekt gespeichert.
 */
