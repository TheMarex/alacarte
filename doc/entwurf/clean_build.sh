#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# clean up old glossary index files
rm entwurfsdokument.glo
rm entwurfsdokument.xdy
# clean up old makeglossaries file
rm entwurfsdokument.gls

pdflatex -halt-on-error -interaction=nonstopmode entwurfsdokument
makeglossaries entwurfsdokument
pdflatex -halt-on-error -interaction=nonstopmode entwurfsdokument

rm *.aux
rm *.log
rm *.toc
rm *.xdy
rm *.gl*
rm *.out
