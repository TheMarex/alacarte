#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# clean up old glossary index files
rm implementierungsdokument.glo
rm implementierungsdokument.xdy
# clean up old makeglossaries file
rm implementierungsdokument.gls

pdflatex -halt-on-error -interaction=nonstopmode implementierungsdokument
makeglossaries implementierungsdokument
pdflatex -halt-on-error -interaction=nonstopmode implementierungsdokument
