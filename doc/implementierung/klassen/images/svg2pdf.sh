#!/bin/sh

if [ -z "$1" ]
then
	for i in $(ls *.svg); do
		echo "$i"
		inkscape -z --file=$i --export-pdf=${i%.svg}.pdf
	done
else

	inkscape -z --file=$1 --export-pdf=${1%.svg}.pdf
fi
