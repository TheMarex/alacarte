#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# clean up old glossary index files
rm pflichtenheft.glo
rm pflichtenheft.xdy
# clean up old makeglossaries file
rm pflichtenheft.gls

pdflatex -halt-on-error -interaction=nonstopmode pflichtenheft
makeglossaries pflichtenheft
pdflatex -halt-on-error -interaction=nonstopmode pflichtenheft

rm *.aux
rm *.log
rm *.toc
rm *.xdy
rm *.gl*
rm *.out
