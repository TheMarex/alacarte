# alaCarte #
alaCarte ist ein Renderer für OpenStreetMap-Kartendaten.
Die gerenderten Kacheln werden über einen Webserver im SlippyMap-URL-Format zur Verfügung gestellt.

Diese Repository ist *veraltet*. Die aktuelle Repository ist auf [Github](https://github.com/TheMarex/alacarte).

## Organisatorisches ##
 * Unsere Betreuer:
    * Veit Batz, Raum 222, batz@kit.edu
    * Dennis Luxen, Raum 220, luxen@kit.edu

## Links ##
 * [Slippy map tilenames](http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
 * [MapCSS](http://wiki.openstreetmap.org/wiki/MapCSS)
 * [boost](http://www.boost.org)
 * [cairo](http://cairographics.org/)
 * [Anti-Grain Geometry](http://www.antigrain.com/articles/ddj1.agdoc.html)
