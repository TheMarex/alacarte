#!/usr/bin/env python
import os

unitTests = ("eval", "general", "importer", "server", "utils", "mapcss", "parser")

for unitTest in unitTests:
	os.system("./unitTests_" + unitTest + " --output_format=XML --log_level=all --report_level=no > unitTests_" + unitTest + ".xml")
